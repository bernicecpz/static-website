module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
    siteUrl: `https://thirsty-northcutt-eb1b9a.netlify.app/`,
    //siteUrl: `https://gatsbystarterdefaultsource.gatsbyjs.io/`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
        {
          site {
            siteMetadata {
              author
              siteUrl
              description
              title
            }
          }
        }
        `,
        feeds: [
          {
            title: "Sample RSS Feed",
            output: "rss.xml",
            query: `
            {             
              allCustomMetaData {
                nodes {
                  id
                  internal {
                    content
                    contentDigest
                    description
                  }
                }
              }
              site {
                siteMetadata {
                  author
                  siteUrl
                  description
                  title
                }
                buildTime
              }
            }
            `,
            serialize: ({ query: { allCustomMetaData, site } }) => {
              const crypto = require('crypto');
              
                  return allCustomMetaData.nodes.map(customNode => {

                      const content = JSON.parse(customNode.internal.content);
                      return Object.assign({}, customNode, {
                        date: site.buildTime,
                        title: content.title,
                        description: customNode.internal.description,
                        url: `${site.siteMetadata.siteUrl}${content.path}`,
                        guid: crypto.randomUUID(),
                      });

                  }) //END of allCustomMetaData

            }, // END OF serialize
          }
        ]
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `projectSrc`,
        path: `${__dirname}/src/pages`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
