// const { createFilePath } = require(`gatsby-source-filesystem`);

// Provided by default Gatsby starer on generating a page into GraphQL data layer
exports.createPages = async ({ actions }) => {
  const { createPage } = actions
  createPage({
    path: "/using-dsg",
    component: require.resolve("./src/templates/using-dsg.js"),
    context: {},
    defer: true,
  })
}

// This allows the creation of custom nodes
exports.sourceNodes = ({ actions, createNodeId, createContentDigest }) => {
  
  // Create the nodes with required information to populate the RSS
  const metaDatas = [
    { path: "/page-2/", title: "Page 2 of my site", description: "Page 2 of default starter website" },
    { path: "/using-typescript/", title: "Using Typescript", description: "Summary of using typescript with Gatsby"}
  ];

  metaDatas.forEach(metaData => {

    const data = JSON.stringify({
      title: metaData.title,
      path: metaData.path
    });

    const nodeMeta = {
      id: createNodeId(`metaData-${metaData.title}`),
      internal: {
        contentDigest: createContentDigest(metaData),
        description: metaData.description,
        content: data,
        type: 'CustomMetaData'
      },
    };
    const node = Object.assign({}, data, nodeMeta)
    actions.createNode(node);
  });
}