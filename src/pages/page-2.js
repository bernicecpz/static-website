import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Header from "../components/header"
import { Helmet } from "react-helmet";


const pageTitle = "Page 2 of my site";
const pageDescription = "Page 2 of default starter website"

const SecondPage = () => (
  <Layout>
    <Seo title="Page two" />
    <Header siteTitle="Page 2" /> 
    <Helmet>
        <meta charSet="utf-8" />
        <title>`${pageTitle}`</title>
        <decription>`${pageDescription}`</decription>
    </Helmet>
    <h1>Hi from the second page</h1>
    <p>Welcome to page 2</p>
    <h2>Dummy Content</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default SecondPage
